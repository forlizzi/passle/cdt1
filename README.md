# Passle CDT - Basic project

Start the repository:

```
git checkout -b "clientId"
yarn
```

For start development:

```
yarn dev
```

For distribution:

```
yarn dist
```

Create CSS from SCSS files:

```
yarn sass
```
