const gulp = require("gulp");
const concat = require("gulp-concat");
const fs = require("fs");
const sass2less = require("sass2less");

const folder = "src/scss/";

const filePath = "./dist/main.scss";
const lessPath = filePath.replace(/\.s[ac]ss$/i, ".less");

[filePath, lessPath].forEach((file) => {
  if (fs.existsSync(file)) {
    fs.unlinkSync(file);
  }
});

const data = fs.readFileSync(folder + "main.scss", "utf-8");
if (!data) {
  return console.error("Invalid main.scss");
}
const files = data
  .trim()
  .replaceAll(/@import "(.*)"/g, "$1")
  .replaceAll("\r", "")
  .replaceAll(";", "")
  .split("\n")
  .map((file) => folder + file);

console.log("Files:", files);

gulp
  .src(files)
  .pipe(concat("main.scss"))
  .pipe(gulp.dest("./dist/"))
  .on("end", () => {
    const buffer = fs.readFileSync(filePath);
    fs.writeFileSync(lessPath, sass2less(buffer.toString()));
  });
